# Changelog
 All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [4.10.2] - 2021-10-09
### Changed
- Increase CHUNK size for write to 65K bytes and force the chunk to be power
of 2 to avoid driver issue

## [4.10.1] - 2021-08-02
### Changed
- Use a specific mutex for attenuators to avoid blocking device when 
attenuator change is made

## [4.10.0] - 2021-06-14
### Changed
- Increase CHUNK size for write to 256 bytes

## [v4.9.1] - 2020-12-18
### Added
- Check if the attenuator value is right on register
### Changed
- Increas the time to change register RELOAD_BOARD_CFG_ADDRESS

## [v4.9.0] - 2020-07-03
### Added
- Read RTM information
- Include Main/Secondary concept
